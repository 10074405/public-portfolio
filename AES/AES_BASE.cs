﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

// Copyright Jack Roden : Bitbucket.org/10074405/

namespace AberUni.Jar55.AES{
    class AES{
        public static byte[] AES_Encrypt(byte[] BTE, byte[] PBStream){
			byte[] EncryptedBytes = null;
			byte[] SaltBytes = PBStream;

			using (MemoryStream MemStr = new MemoryStream()){
				using (RijndaelManaged AES = new RijndaelManaged()){
					AES.KeySize = 256;
					AES.BlockSize = 128;

					var key = new Rfc2898DeriveBytes(PBStream, SaltBytes, 1000);
					AES.Key = key.GetBytes(AES.KeySize / 8);
					AES.IV = key.GetBytes(AES.BlockSize / 8);

					AES.Mode = CipherMode.CBC;

					using (CryptoStream CS = new CryptoStream(MemStr, AES.CreateEncryptor(), CryptoStreamMode.Write)){
						CS.Write(BTE, 0, BTE.Length);
						CS.Close();
					}
					
					EncryptedBytes = MemStr.ToArray();
				}
			}
			return EncryptedBytes;
		}

        public static byte[] AES_Decrypt(byte[] BTD, byte[] PBStream){
            byte[] DecryptedBytes = null;
            byte[] SaltBytes = PBStream;
			
            using (MemoryStream MemStr = new MemoryStream()){
                using (RijndaelManaged AES = new RijndaelManaged()){
                    AES.KeySize = 256;
                    AES.BlockSize = 128;

                    var key = new Rfc2898DeriveBytes(PBStream, SaltBytes, 1000);
                    AES.Key = key.GetBytes(AES.KeySize / 8);
                    AES.IV = key.GetBytes(AES.BlockSize / 8);

                    AES.Mode = CipherMode.CBC;

                    using (CryptoStream CS = new CryptoStream(MemStr, AES.CreateDecryptor(), CryptoStreamMode.Write)){
                        CS.Write(BTD, 0, BTD.Length);
                        CS.Close();
                    }
                    DecryptedBytes = MemStr.ToArray();
                }
            }

            return DecryptedBytes;
        }

        public static string Encrypt(string text, byte[] PBStream){
            byte[] OGBytes = Encoding.UTF8.GetBytes(text);
            byte[] EncryptedBytes = null;

            
            PBStream = SHA256.Create().ComputeHash(PBStream);				// Lets hash the password with SHA256
            int SaltSize = GetSaltSize(PBStream);							// Get the salt size 
            byte[] SB = GetRandomBytes(SaltSize);

            byte[] BTE = new byte[SB.Length + OGBytes.Length];
			
            for (int i = 0; i < SB.Length; i++){
                BTE[i] = SB[i];
            }
			
            for (int i = 0; i < OGBytes.Length; i++){
                BTE[i + SB.Length] = OGBytes[i];
            }

            EncryptedBytes = AES_Encrypt(BTE, PBStream);

            return Convert.ToBase64String(EncryptedBytes);
        }

        public static string Decrypt(string decryptedText, byte[] PBStream){
            byte[] BTD = Convert.FromBase64String(decryptedText);

            PBStream = SHA256.Create().ComputeHash(PBStream);				// Lets hash the password with SHA256
            byte[] DecryptedBytes = AES_Decrypt(BTD, PBStream);
            int SaltSize = GetSaltSize(PBStream);							// Get the size of the salt

            byte[] OGBytes = new byte[DecryptedBytes.Length - SaltSize];	// Losing Salt bytes, getting originals
			
            for (int i = SaltSize; i < DecryptedBytes.Length; i++){
                OGBytes[i - SaltSize] = DecryptedBytes[i];
            }

            return Encoding.UTF8.GetString(OGBytes);
        }

        public static int GetSaltSize(byte[] PBStream){
            var key = new Rfc2898DeriveBytes(PBStream, PBStream, 1000);
            byte[] BA = key.GetBytes(2);	// Like Baracus, Except more byte-y
            StringBuilder STR = new StringBuilder();
			
            for (int i = 0; i < BA.Length; i++){
                STR.Append(Convert.ToInt32(BA[i]).ToString());
            }
			
            int SaltSize = 0;
            string S = STR.ToString();
			
            foreach (char C in S){
                int IntChar = Convert.ToInt32(C.ToString());
                SaltSize = SaltSize + IntChar;
            }

            return SaltSize;
        }

        public static byte[] GetRandomBytes(int length){
            byte[] BA = new byte[length];
            RNGCryptoServiceProvider.Create().GetBytes(BA);
            return BA;
        }
    }
}
