﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

// Copyright Jack Roden : Bitbucket.org/10074405/

namespace AberUni.Jar55.AES{
	class SetSalt{
		// Ideally this is in a seperated file 
		// and called to using byte[] PWB = GetPasswordBytes();
		// Then when AES.Encrypt/Decrypt are called its passed in and 
		// the system does the rest.

		string TMP = "HelloWorld";

		private byte[] GetPasswordBytes(){
			// The real password characters is stored in System.SecureString
			// Below is code that shows converting System.SecureString into Byte[]
			byte[] BA = null;

			if (TMP.Length == 0)
				BA = new byte[] { 4, 1, 2, 0, 1, 2, 1, 5 };
			else{
				// Convert System.SecureString to Pointer
				IntPtr UMB = Marshal.SecureStringToGlobalAllocAnsi(TMP);
				try{
					// You have to mark your application to allow unsafe code
					// Enable it at Properties > Build (Visual Studio 13)
					unsafe{
						byte* byteArray = (byte*)UMB.ToPointer();

						// Get the end of the string
						byte* pEnd = byteArray;
						while (*pEnd++ != 0) {
							// We'll Do something here later... but err.. for now lets chill
						}

						int length = (int)((pEnd - byteArray) - 1);

						BA = new byte[length];

						for (int i = 0; i < length; ++i){
							byte dataAtIndex = *(byteArray + i);
							BA[i] = dataAtIndex;
						}
					}
				}
				finally{
					Marshal.ZeroFreeGlobalAllocAnsi(UMB);	// Remove it completely from memory, Leave no trace.. Like Agent 47 or solid snake..
				}
			}

			return System.Security.Cryptography.SHA256.Create().ComputeHash(BA);
		}
	}
}